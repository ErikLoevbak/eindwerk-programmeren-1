package beans;

public class Soort {

    private int nr;
    private String soortNaam;

    public Soort() {
    }

    public int getNr() {
        return nr;
    }

    public String getSoortNaam() {
        return soortNaam;
    }

    public void setNr(int nr) {
        this.nr = nr;
    }

    public void setSoortNaam(String soortNaam) {
        this.soortNaam = soortNaam;
    }

    @Override
    public String toString() {
        return "Nr: " + nr + "\nSoort: " + soortNaam;
    }

}

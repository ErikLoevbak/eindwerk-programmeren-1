package beans;

public class Lener {

    private int nr;
    private String lenerNaam;
    private String straat;
    private String huisNr;
    private String busNr;
    private int postCode;
    private String gemeente;
    private String telefoon;
    private String email;

    public Lener() {
    }

    public int getNr() {
        return nr;
    }

    public String getLenerNaam() {
        return lenerNaam;
    }

    public String getStraat() {
        return straat;
    }

    public String getHuisNr() {
        return huisNr;
    }

    public String getBusNr() {
        return busNr;
    }

    public int getPostCode() {
        return postCode;
    }

    public String getGemeente() {
        return gemeente;
    }

    public String getTelefoon() {
        return telefoon;
    }

    public String getEmail() {
        return email;
    }

    public void setNr(int nr) {
        this.nr = nr;
    }

    public void setLenerNaam(String lenerNaam) {
        this.lenerNaam = lenerNaam;
    }

    public void setStraat(String straat) {
        this.straat = straat;
    }

    public void setHuisNr(String huisNr) {
        this.huisNr = huisNr;
    }

    public void setBusNr(String busNr) {
        this.busNr = busNr;
    }

    public void setPostCode(int postCode) {
        this.postCode = postCode;
    }

    public void setGemeente(String gemeente) {
        this.gemeente = gemeente;
    }

    public void setTelefoon(String telefoon) {
        this.telefoon = telefoon;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public String showNaamGemeente(){
        return "Lener: \nNaam: " + lenerNaam + "\nGemeente: " + gemeente;
    }
    
    public String showNGTE(){
        return "\nNaam: " + lenerNaam + "\nGemeente: " + gemeente + "\nTelefoon: " + telefoon + "\nEmail: " + email;
    }
    
    @Override
    public String toString() {
        return "Lener{" + "nr=" + nr + ", lenerNaam=" + lenerNaam + ", straat=" + straat + ", huisNr=" + huisNr + ", busNr=" + busNr + ", postCode=" + postCode + ", gemeente=" + gemeente + ", telefoon=" + telefoon + ", email=" + email + '}';
    }
    
    public String showNrNaam(){
        return nr + " " + lenerNaam;
    }
}

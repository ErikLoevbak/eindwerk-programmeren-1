package beans;

import java.util.Date;

public class Uitlening {

    private int nr;
    private int spelNr;
    private int lenerNr;
    private Date uitleenDatum;
    private Date terugBrengDatum;
    private Lener lener;
    private Spel spel;

    public Uitlening() {
    }

    public int getNr() {
        return nr;
    }

    public int getSpelNr() {
        return spelNr;
    }

    public int getLenerNr() {
        return lenerNr;
    }

    public Date getUitleenDatum() {
        return uitleenDatum;
    }

    public Date getTerugBrengDatum() {
        return terugBrengDatum;
    }

    public Lener getLener() {
        return lener;
    }

    public Spel getSpel() {
        return spel;
    }

    public void setNr(int nr) {
        this.nr = nr;
    }

    public void setSpelNr(int spelNr) {
        this.spelNr = spelNr;
    }

    public void setLenerNr(int lenerNr) {
        this.lenerNr = lenerNr;
    }

    public void setUitleenDatum(Date uitleenDatum) {
        this.uitleenDatum = uitleenDatum;
    }

    public void setTerugBrengDatum(Date terugBrengDatum) {
        this.terugBrengDatum = terugBrengDatum;
    }

    public void setLener(Lener lener) {
        this.lener = lener;
    }

    public void setSpel(Spel spel) {
        this.spel = spel;
    }
    
    public String spelNaam(){
        return spel.getNaam();
    }
    
    public void showUitlening(){
        System.out.printf("%-32s\t%-20s\t%-15s\t%-15s%n",spel.getNaam(), lener.getLenerNaam(), getUitleenDatum(), getTerugBrengDatum());
    }

    @Override
    public String toString() {
        return "Uitlening{" + "nr=" + nr + ", spelNr=" + spelNr + ", lenerNr=" + lenerNr + ", uitleenDatum=" + uitleenDatum + ", terugBrengDatum=" + terugBrengDatum + ", lener=" + lener + ", spel=" + spel + '}';
    }

}

package beans;

public class Moeilijkheid {

    private int nr;
    private String moeilijkheidsNaam;

    public Moeilijkheid() {
    }

    public void setNr(int nr) {
        this.nr = nr;
    }

    public void setMoeilijkheidsNaam(String moeilijkheidsNaam) {
        this.moeilijkheidsNaam = moeilijkheidsNaam;
    }

    public int getNr() {
        return nr;
    }

    public String getMoeilijkheidsNaam() {
        return moeilijkheidsNaam;
    }

    @Override
    public String toString() {
        return "Moeilijkheid{" + "nr=" + nr + ", moeilijkheidsNaam=" + moeilijkheidsNaam + '}';
    }

}

package beans;

import java.util.Date;

public class Spel {

    private int nr;
    private String naam;
    private String uitgever;
    private String auteur;
    private int jaarUitgifte;
    private String leeftijd;
    private int min_Spelers;
    private int max_Spelers;
    private int soortNr;
    private String speelDuur;
    private int moeilijkheidNr;
    private double prijs;
    private String afbeelding;
    private Soort soort;
    private Moeilijkheid moeilijkheid;

    public Spel() {
    }

    public int getNr() {
        return nr;
    }

    public String getNaam() {
        return naam;
    }

    public String getUitgever() {
        return uitgever;
    }

    public String getAuteur() {
        return auteur;
    }

    public int getJaarUitgifte() {
        return jaarUitgifte;
    }

    public String getLeeftijd() {
        return leeftijd;
    }

    public int getMin_Spelers() {
        return min_Spelers;
    }

    public int getMax_Spelers() {
        return max_Spelers;
    }

    public int getSoortNr() {
        return soortNr;
    }

    public String getSpeelDuur() {
        return speelDuur;
    }

    public int getMoeilijkheidNr() {
        return moeilijkheidNr;
    }

    public double getPrijs() {
        return prijs;
    }

    public String getAfbeelding() {
        return afbeelding;
    }

    public Soort getSoort() {
        return soort;
    }

    public Moeilijkheid getMoeilijkheid() {
        return moeilijkheid;
    }

    public void setNr(int nr) {
        this.nr = nr;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public void setUitgever(String uitgever) {
        this.uitgever = uitgever;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public void setJaarUitgifte(int jaarUitgifte) {
        this.jaarUitgifte = jaarUitgifte;
    }

    public void setLeeftijd(String leeftijd) {
        this.leeftijd = leeftijd;
    }

    public void setMin_Spelers(int min_Spelers) {
        this.min_Spelers = min_Spelers;
    }

    public void setMax_Spelers(int max_Spelers) {
        this.max_Spelers = max_Spelers;
    }

    public void setSoortNr(int soortNr) {
        this.soortNr = soortNr;
    }

    public void setSpeelDuur(String speelDuur) {
        this.speelDuur = speelDuur;
    }

    public void setMoeilijkheidNr(int moeilijkheidNr) {
        this.moeilijkheidNr = moeilijkheidNr;
    }

    public void setPrijs(double prijs) {
        this.prijs = prijs;
    }

    public void setAfbeelding(String afbeelding) {
        this.afbeelding = afbeelding;
    }

    public void setSoort(Soort soort) {
        this.soort = soort;
    }

    public void setMoeilijkheid(Moeilijkheid moeilijkheid) {
        this.moeilijkheid = moeilijkheid;
    }

    public String showSpel() {
        return "Spel:              |" + naam + "\nUitgever:          |" + uitgever + "\nLeeftijdsgroep:    |" + leeftijd + "\nKostprijs:         |" + prijs + "\nAfbeelding:        |" + afbeelding + "\n" ;
    }
    public String showSpelDiff() {
        return "Spel:              |" + naam + "\nUitgever:          |" + uitgever + "\nLeeftijdsgroep:    |" + leeftijd + "\nKostprijs:         |" + prijs + "\nAfbeelding:        |" + afbeelding + "\nMoeilijkheid:      |" + moeilijkheid.getMoeilijkheidsNaam() + "\n" ;
    }
    
    @Override
    public String toString() {
        return "Naam: " + naam + "\nUitgever: " + uitgever + "\nPrijs: " + prijs + "\n\n ";
    }

}

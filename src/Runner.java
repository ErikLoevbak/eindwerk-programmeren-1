
import beans.Lener;
import beans.Soort;
import beans.Spel;
import beans.Uitlening;
import dataaccess.DALener;
import dataaccess.DASoort;
import dataaccess.DASpel;
import dataaccess.DAUitlening;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Runner {

    public static void main(String[] args) throws SQLException {
        String choice;
        int exit = 1;
        String diffKeuze;
        boolean proceed = false;
        String monthChoice;
        Spel spel11;
        int spelid = 0;
        int lenerid = 0;

        DALener dalener = new DALener();
        DASpel daSpel = new DASpel();
        DAUitlening daUitlening = new DAUitlening();
        DASoort dasoort = new DASoort();

        Scanner scanner = new Scanner(System.in);
        Scanner spelScan = new Scanner(System.in);
        Scanner scanner7 = new Scanner(System.in);
        Scanner scanner9 = new Scanner(System.in);
        Scanner scanner11 = new Scanner(System.in);
        do {
            System.out.println("Welkom bij Erik's spellenverhuur!");
            System.out.println("Will you play the game or will the game play YOU?!");
            System.out.println("--------------------------------------------------");
            System.out.println(" ");
            System.out.println("KIES EEN OPTIE");
            System.out.println("  1. Toon de eerste spelsoort");
            System.out.println("  2. Toon het vijfde spel");
            System.out.println("  3. Toon de eerste lener");
            System.out.println("  4. Toon een spel naar keuze");
            System.out.println("  5. Toon alle spellen");
            System.out.println("  6. Toon een lijst met spellen en kies een spel");
            System.out.println("  7. Toon uitleningen");
            System.out.println("  8. Uitgebreid zoeken: moeilijkheid");
            System.out.println("  9. Uitgebreid zoeken: leners");
            System.out.println("  10.Toon de uitgeleende spellen tijdens een bepaalde maand.");
            System.out.println("  11.Administratie Uitleningen\n");
            System.out.println("Druk op een getal tussen 1 en 11 om verder te gaan of 0 om te stoppen.");
            choice = scanner.next();
            switch (choice) {
                case "1":
                    Soort soort = dasoort.findSoort();
                    do {
                        System.out.println("\n" + soort.toString());
                        System.out.println(" ");
                        System.out.println("Wilt u:");
                        System.out.println("1. Opnieuw tonen");
                        System.out.println("2. Het programma afsluiten");
                        System.out.println("3. Opnieuw beginnen");
                        System.out.println(" ");
                        System.out.println("Kies '1' of '2'.");
                        exit = scanner.nextInt();
                    } while (exit == 1);
                    if (exit == 2) {
                        choice = "0";
                    }
                    break;
                case "2":
                    Spel spel = daSpel.findSpel();
                    System.out.println("\n" + spel.toString());
                    System.out.println(" ");
                    break;
                case "3":
                    Lener lener = dalener.findLener();
                    System.out.println("\n" + lener.showNaamGemeente());
                    System.out.println(" ");
                    break;
                case "4":
                    System.out.println("Welk spel wilt u zien?");
                    String userInput = spelScan.nextLine();
                    userInput = userInput.toUpperCase();
                    Spel spel2 = daSpel.searchSpel(userInput);
                    if (spel2 != null) {
                        System.out.println(spel2.showSpel());
                    } else {
                        System.out.print("\nDit spel werd niet gevonden bij ons. ");
                    }
                    break;
                case "5":
                    ArrayList<Spel> spelList = daSpel.findSpelList();
                    for (Spel spelPrint : spelList) {
                        System.out.println(spelPrint);
                    }
                    break;
                case "6":
                    ArrayList<Spel> spelList2 = daSpel.findSpelList();
                    for (Spel spelPrint : spelList2) {
                        System.out.println(spelPrint);
                    }
                    System.out.println("Over welk spel wenst u meer informatie?");
                    userInput = spelScan.nextLine();
                    userInput = userInput.toUpperCase();
                    spel = daSpel.searchSpel(userInput);
                    if (spel != null) {
                        System.out.println(spel.showSpel());
                    } else {
                        System.out.print("\nDit spel werd niet gevonden bij ons. ");
                    }
                    break;
                case "7":
                    System.out.printf("%-32s\t%-20s\t%-15s\t%-15s\n\n", "Naam Spel", "Naam Lener", "UitleenDatum", "TerugbrengDatum");
                    ArrayList<Uitlening> alleLeningLijst = daUitlening.getUitleningList();
                    for (Uitlening alleLeenPrint : alleLeningLijst) {
                        alleLeenPrint.showUitlening();
                    }
                    System.out.println("\nOp welke lener wenst u te filteren?");
                    String leennaam = scanner7.nextLine().toLowerCase();
                    ArrayList<Uitlening> leningLijst = daUitlening.searchUitleningList(leennaam);

                    if (leningLijst.size() > 0) {
                        System.out.printf("%-32s\t%-20s\t%-15s\t%-15s\n\n", "Naam Spel", "Naam Lener", "UitleenDatum", "TerugbrengDatum");
                        for (Uitlening leenPrint : leningLijst) {
                            leenPrint.showUitlening();
                        }
                    } else {
                        System.out.println("\nEr zijn geen leningen onder deze naam.\n");
                    }
                    break;
                case "8":
                    do {
                        System.out.println("Geef de laagste moeilijkheid");
                        System.out.println("1. heel simpel");
                        System.out.println("2. eenvoudig");
                        System.out.println("3. moeilijker");
                        System.out.println("4. gemiddeld");
                        System.out.println("5. moeilijk");
                        System.out.println("Maak uw keuze.");
                        diffKeuze = scanner.next();
                        switch (diffKeuze) {
                            case "1":
                            case "2":
                            case "3":
                            case "4":
                            case "5":
                                proceed = true;
                                break;
                            default:
                                System.out.println("\nGelieve een getal in te geven tussen '1' en '5'.\n");
                        }
                    } while (proceed == false);
                    ArrayList<Spel> spellenLijst = daSpel.findSpelMoeilijkheid(Integer.parseInt(diffKeuze));
                    for (Spel printSpel : spellenLijst) {
                        System.out.println(printSpel.showSpelDiff());
                    }
                    break;
                case "9":
                    System.out.println("\nGeef (een deel van) de naam van de lener in.");
                    userInput = scanner9.nextLine().toLowerCase();
                    ArrayList<Lener> lenerLijst = dalener.getLeners(userInput);
                    if (lenerLijst.size() > 0) {
                        for (Lener lenerOut : lenerLijst) {
                            System.out.println(lenerOut.showNGTE());
                        }
                        
                    } else {
                        System.out.println("\nEr zijn geen leners onder deze naam.\n");
                    }

                    break;
                case "10":
                    System.out.println("1. Januari");
                    System.out.println("2. Februari");
                    System.out.println("3. Maart");
                    System.out.println("4. April");
                    System.out.println("5. Mei");
                    System.out.println("6. Juni");
                    System.out.println("7. Juli");
                    System.out.println("8. Augustus");
                    System.out.println("9. September");
                    System.out.println("10.Oktober");
                    System.out.println("11.November");
                    System.out.println("12.December");
                    do {
                        System.out.println("\nKies de maand waarvan u een overzicht wil.");
                        Scanner scan10 = new Scanner(System.in);
                        monthChoice = scan10.next();

                        switch (monthChoice) {
                            case "1":
                            case "2":
                            case "3":
                            case "4":
                            case "5":
                            case "6":
                            case "7":
                            case "8":
                            case "9":
                            case "10":
                            case "11":
                            case "12":
                                proceed = true;
                                break;
                            default:
                                System.out.println("\nGelieve een getal overeenkomstig met een maand in te geven.\n");
                                break;
                        }
                    } while (proceed == false);

                    ArrayList<Uitlening> geleendLijst = daUitlening.getUitgeleend();
                    ArrayList<Uitlening> geleendLijstf = new ArrayList<>();
                    for (Uitlening x : geleendLijst) {
                        String[] splitX = x.getUitleenDatum().toString().split("-");
                        String month = splitX[1];
                        if (Integer.parseInt(monthChoice) == Integer.parseInt(month)) {
                            geleendLijstf.add(x);
                        }
                    }
                    if (geleendLijstf.size() > 0) {
                        for (Uitlening uitlening2 : geleendLijstf) {
                            System.out.println(uitlening2.spelNaam());
                        }
                    } else {
                        System.out.println("\nEr waren geen uitleningen tijdens deze maand.\n");
                    }
                    break;
                case "11":
                    ArrayList<Spel> spellen = daSpel.findSpelListByNr();
                    System.out.println("\n-------------Spellen-------------");
                    for (Spel case11 : spellen) {
                        System.out.println(case11.getNr() + ": " + case11.getNaam());
                    }
                    System.out.println("\nKies een spel door het ID in te geven.");

                    boolean proceed2 = false;
                    while (proceed2 == false) {
                        while (!scanner11.hasNextInt()) {
                            String spelidS = scanner11.next();
                            System.out.println("Dit is geen getal. Gelieve een geldig getal in te geven.");
                        }
                        spelid = scanner11.nextInt();
                        spel11 = daSpel.searchSpelonID(spelid);
                        if (spel11 == null) {
                            System.out.println("Dit is geen geldig ID, probeer opnieuw.");
                        } else {
                            System.out.println("\n" + spel11.showSpel());
                            proceed2 = true;
                        }
                    }

                    System.out.println("Geef nu de lener aan, aan de hand van lenernummer.");
                    System.out.println("\nTyp 'lener' voor een ID-lijst met alle leners.");
                    proceed2 = false;
                    while (proceed2 == false) {
                        while (!scanner11.hasNextInt()) {
                            String leneridS = scanner11.next();
                            if (leneridS.equals("lener")) {
                                ArrayList<Lener> showLeners = dalener.lenerIdList();
                                for (Lener lenerId : showLeners) {
                                    System.out.println(lenerId.showNrNaam());
                                }
                                System.out.println("\nGeef nu uw keuze aan.");
                            } else {
                                System.out.println("Dit is geen getal. Gelieve een geldig getal in te geven.");
                            }
                        }
                        lenerid = scanner11.nextInt();
                        Lener lener11 = dalener.idCheck(lenerid);
                        if (lener11 == null) {
                            System.out.println("Er bestaat geen lener met dit ID-nummer. Gelieve een geldig nummer in te geven.");
                            System.out.println("\nIndien u een lijst wil zien van leners, druk typ 'lener'");
                        } else {
                            proceed2 = true;
                        }
                    }
                    daUitlening.addUitlening(spelid, lenerid);
                    ArrayList<Uitlening> uitLeenLijst = daUitlening.getUitleningList();
                    System.out.printf("%-32s\t%-20s\t%-15s\t%-15s\n", "Naam Spel", "Naam Lener", "UitleenDatum", "TerugbrengDatum");
                    for (Uitlening lening : uitLeenLijst) {
                        lening.showUitlening();
                    }

                    break;

                case "0":
                    break;
                default:
                    System.out.println("\n\n\n\n\nGelieve een getal tussen 0 en 11 in te geven.");
                    break;
            }

            if (!(choice.equals("0"))) {
                try {
                    System.out.println("Druk op ENTER om opnieuw te beginnen.");
                    int read = System.in.read(new byte[2]);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                for (int i = 0; i < 50; i++) {
                    System.out.println("\n");
                }
            }
        } while (!(choice.equals("0")));
        try {
            System.out.println("Druk op ENTER om het programma af te sluiten.");
            int read = System.in.read(new byte[2]);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

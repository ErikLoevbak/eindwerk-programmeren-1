package dataaccess;

import beans.Soort;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DASoort {
    private static final String URL = "jdbc:oracle:thin:@localhost:1521:XE";
    private static final String LOGIN = "admin";
    private static final String PASSWORD = "sql";
    private static final String DRIVER = "oracle.jdbc.driver.oracleDriver";

    private Connection createConnection() throws SQLException {
        return DriverManager.getConnection(URL, LOGIN, PASSWORD);

    }
    public Soort findSoort() throws SQLException {
        try (Connection connection = createConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("Select * from Soort where nr = 1");

            if (resultSet.next()) {
                Soort soort = new Soort();
                soort.setNr(resultSet.getInt("nr"));
                soort.setSoortNaam(resultSet.getString("soortnaam"));
                return soort;
            } else {
                return null;
            }

        } catch (SQLException e) {
            throw new SpelException("Soort could not be found.", e);
        }
    }
}

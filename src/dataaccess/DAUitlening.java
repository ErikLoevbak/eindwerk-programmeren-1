package dataaccess;

import beans.Lener;
import beans.Spel;
import beans.Uitlening;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DAUitlening {
    private static final String URL = "jdbc:oracle:thin:@localhost:1521:XE";
    private static final String LOGIN = "admin";
    private static final String PASSWORD = "sql";
    private static final String DRIVER = "oracle.jdbc.driver.oracleDriver";

    private Connection createConnection() throws SQLException {
        return DriverManager.getConnection(URL, LOGIN, PASSWORD);

    }
    
    public ArrayList <Uitlening> searchUitleningList(String leennaam) throws SQLException {
        ArrayList <Uitlening> uitleningLijst = new ArrayList <>();
        try (Connection connection = createConnection()) {
            PreparedStatement pstatement = connection.prepareStatement("Select * from uitlening inner join lener on uitlening.lenernr = lener.nr inner join spel on uitlening.spelnr = spel.nr where lower(lener.lenernaam) like ? order by spel.naam asc, uitlening.uitleendatum asc");
            pstatement.setString(1, "%" + leennaam + "%");
            ResultSet resultSet = pstatement.executeQuery();
            
            while (resultSet.next()) {
                Uitlening uitlening = new Uitlening();
                uitlening.setUitleenDatum(resultSet.getDate("uitleenDatum"));
                uitlening.setTerugBrengDatum(resultSet.getDate("terugbrengdatum"));
                
                Lener lener = new Lener();
                lener.setLenerNaam(resultSet.getString("lenernaam"));

                uitlening.setLener(lener);
                
                Spel spel = new Spel();
                spel.setNaam(resultSet.getString("naam"));
                
                uitlening.setSpel(spel);
            
                uitleningLijst.add(uitlening);
            }
            return uitleningLijst;
        }
        catch (SQLException e) {
            throw new SpelException ("Uitlening could not be found.", e);
        }
    }
    
    public ArrayList <Uitlening> getUitleningList() throws SQLException {
        ArrayList <Uitlening> uitleningLijst = new ArrayList <>();
        try (Connection connection = createConnection()) {
            PreparedStatement pstatement = connection.prepareStatement("Select * from uitlening inner join lener on uitlening.lenernr = lener.nr inner join spel on uitlening.spelnr = spel.nr order by spel.naam asc, uitlening.uitleendatum asc");
            ResultSet resultSet = pstatement.executeQuery();
            while (resultSet.next()) {
                Uitlening uitlening = new Uitlening();
                uitlening.setUitleenDatum(resultSet.getDate("uitleenDatum"));
                uitlening.setTerugBrengDatum(resultSet.getDate("terugbrengdatum"));
                
                Lener lener = new Lener();
                lener.setLenerNaam(resultSet.getString("lenernaam"));

                uitlening.setLener(lener);
                
                Spel spel = new Spel();
                spel.setNaam(resultSet.getString("naam"));
                
                uitlening.setSpel(spel);
            
                uitleningLijst.add(uitlening);
            }
            return uitleningLijst;
        }
        catch (SQLException e) {
            throw new SpelException ("Uitlening could not be found.", e);
        }
    }
    
    private static java.sql.Date getCurrentDate(){
        java.util.Date today = new java.util.Date();
        return new java.sql.Date(today.getTime());
    }
    
    public void addUitlening(int spelNr, int lenerNr) throws SQLException{
        Uitlening uitlening = new Uitlening();
        try (Connection connection = createConnection()){
            
            PreparedStatement pstatement = connection.prepareStatement("insert into uitlening values (uitlening_seq.nextval, ?, ?, ?, null)");
            pstatement.setInt(1, spelNr);
            pstatement.setInt(2, lenerNr);
            pstatement.setDate(3,getCurrentDate());
            pstatement.executeUpdate();
            
        }
        catch (SQLException e) {
            throw new SpelException("Unable to perform this action", e);
        }
    }
    
    public ArrayList <Uitlening> getUitgeleend() throws SQLException{
        ArrayList <Uitlening> arrayList = new ArrayList <> ();
        try (Connection connection = createConnection()){
            PreparedStatement pstatement = connection.prepareStatement("select * from uitlening inner join spel on uitlening.spelnr = spel.nr order by uitleendatum");
            ResultSet resultSet = pstatement.executeQuery();
            while (resultSet.next()){
                Uitlening uitlening = new Uitlening();
                uitlening.setUitleenDatum(resultSet.getDate("uitleendatum"));
                
                Spel spel = new Spel();
                spel.setNaam(resultSet.getString("naam"));
                
                uitlening.setSpel(spel);
                
                arrayList.add(uitlening);
            }
            return arrayList;
        }
        catch (SQLException e){
            throw new SpelException("Dit kon niet worden teruggevonden", e);
        }
    }
}

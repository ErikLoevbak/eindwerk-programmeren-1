package dataaccess;

public class SpelException extends RuntimeException{

    public SpelException(String message, Throwable cause) {
        super(message, cause);
    }
    
}

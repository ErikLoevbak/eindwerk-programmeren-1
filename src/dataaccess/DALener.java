package dataaccess;

import beans.Lener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DALener {

    private static final String URL = "jdbc:oracle:thin:@localhost:1521:XE";
    private static final String LOGIN = "admin";
    private static final String PASSWORD = "sql";
    private static final String DRIVER = "oracle.jdbc.driver.oracleDriver";

    private Connection createConnection() throws SQLException {
        return DriverManager.getConnection(URL, LOGIN, PASSWORD);
    }

    public DALener() {
    }

    public Lener findLener() throws SQLException {
        try (Connection connection = createConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("Select * from lener where nr = 1");

            if (resultSet.next()) {
                Lener lener = new Lener();
                lener.setNr(resultSet.getInt("nr"));
                lener.setLenerNaam(resultSet.getString("lenernaam"));
                lener.setStraat(resultSet.getString("straat"));
                lener.setHuisNr(resultSet.getString("Huisnr"));
                lener.setBusNr(resultSet.getString("Busnr"));
                lener.setPostCode(resultSet.getInt("Postcode"));
                lener.setGemeente(resultSet.getString("Gemeente"));
                lener.setTelefoon(resultSet.getString("telefoon"));
                lener.setEmail(resultSet.getString("email"));

                return lener;
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new SpelException("Lener could not be found", e);
        }
    }

    public ArrayList<Lener> getLeners(String userInput) throws SQLException {
        ArrayList<Lener> lenerList = new ArrayList<>();
        try (Connection connection = createConnection()) {
            PreparedStatement pstatement = connection.prepareStatement("select lenernaam, gemeente, telefoon, email from Lener where lower(lenernaam) like ?");
            pstatement.setString(1, "%" + userInput + "%");
            ResultSet resultSet = pstatement.executeQuery();

            try {
                while (resultSet.next()) {
                    Lener lener = new Lener();
                    lener.setLenerNaam(resultSet.getString("lenernaam"));
                    lener.setGemeente(resultSet.getString("gemeente"));
                    lener.setTelefoon(resultSet.getString("telefoon"));
                    lener.setEmail(resultSet.getString("email"));

                    lenerList.add(lener);
                }
            } catch (SpelException e) {
                return null;
            }
            return lenerList;
        } catch (SQLException e) {
            throw new SQLException("Record could not be found", e);
        }
    }

    public Lener idCheck(int id) throws SQLException {
        try (Connection connection = createConnection()) {
            PreparedStatement pstatement = connection.prepareStatement("select lenernaam from lener where nr like ?");
            pstatement.setInt(1, id);
            ResultSet resultSet = pstatement.executeQuery();
            if (resultSet.next()) {
                Lener lener = new Lener();
                lener.setLenerNaam(resultSet.getString("lenernaam"));
                return lener;
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new SpelException("Kon niet gevonden worden", e);
        }
    }
    public ArrayList <Lener> lenerIdList() throws SQLException{
        ArrayList<Lener> leners = new ArrayList<>();
        try (Connection connection = createConnection()){
            PreparedStatement pstatement = connection.prepareStatement("Select lenernaam, nr from lener order by nr");
            ResultSet resultSet = pstatement.executeQuery();
            while(resultSet.next()){
                Lener lener = new Lener();
                lener.setNr(resultSet.getInt("nr"));
                lener.setLenerNaam(resultSet.getString("lenernaam"));
                
                leners.add(lener);
            }
            return leners;
        }
        catch (SQLException e){
            throw new SpelException("Op dit ogenblik zijn er geen leners.", e);
        }
    }
}

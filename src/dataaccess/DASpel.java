package dataaccess;

import beans.Moeilijkheid;
import beans.Soort;
import beans.Spel;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DASpel {

    private static final String URL = "jdbc:oracle:thin:@localhost:1521:XE";
    private static final String LOGIN = "admin";
    private static final String PASSWORD = "sql";
    private static final String DRIVER = "oracle.jdbc.driver.oracleDriver";

    private Connection createConnection() throws SQLException {
        return DriverManager.getConnection(URL, LOGIN, PASSWORD);

    }

    public Spel findSpel() throws SQLException {
        try (Connection connection = createConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("Select * from Spel inner join Soort on spel.soortnr = soort.nr inner join moeilijkheid on spel.moeilijkheidnr = moeilijkheid.nr where spel.nr = 5");

            if (resultSet.next()) {
                Spel spel = new Spel();
                spel.setNr(resultSet.getInt("nr"));
                spel.setNaam(resultSet.getString("naam"));
                spel.setUitgever(resultSet.getString("Uitgever"));
                spel.setAuteur(resultSet.getString("Auteur"));
                spel.setJaarUitgifte(resultSet.getInt("Jaar_uitgifte"));
                spel.setLeeftijd(resultSet.getString("leeftijd"));
                spel.setMin_Spelers(resultSet.getInt("min_Spelers"));
                spel.setMax_Spelers(resultSet.getInt("max_Spelers"));
                spel.setSoortNr(resultSet.getInt("Soortnr"));
                spel.setSpeelDuur(resultSet.getString("speelduur"));
                spel.setMoeilijkheidNr(resultSet.getInt("moeilijkheidnr"));
                spel.setPrijs(resultSet.getDouble("prijs"));
                spel.setAfbeelding(resultSet.getString("afbeelding"));

                Soort soort = new Soort();
                soort.setNr(resultSet.getInt("nr"));
                soort.setSoortNaam(resultSet.getString("soortnaam"));

                spel.setSoort(soort);

                Moeilijkheid moeilijkheid = new Moeilijkheid();
                moeilijkheid.setNr(resultSet.getInt("nr"));
                moeilijkheid.setMoeilijkheidsNaam(resultSet.getString("moeilijkheidnaam"));
                
                spel.setMoeilijkheid(moeilijkheid);
                return spel;
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new SpelException("Game could not be found.", e);
        }
    }
    public Spel searchSpel(String userInput) throws SQLException {
        try (Connection connection = createConnection()) {
            PreparedStatement pstatement = connection.prepareStatement("Select * from Spel inner join Soort on spel.soortnr = soort.nr inner join moeilijkheid on spel.moeilijkheidnr = moeilijkheid.nr where Upper(spel.naam)  like ?");
            pstatement.setString(1, "%" + userInput + "%");
            ResultSet resultSet = pstatement.executeQuery();
            if (resultSet.next()) {
                Spel spel = new Spel();
                spel.setNr(resultSet.getInt("nr"));
                spel.setNaam(resultSet.getString("naam"));
                spel.setUitgever(resultSet.getString("Uitgever"));
                spel.setAuteur(resultSet.getString("Auteur"));
                spel.setJaarUitgifte(resultSet.getInt("Jaar_uitgifte"));
                spel.setLeeftijd(resultSet.getString("leeftijd"));
                spel.setMin_Spelers(resultSet.getInt("min_Spelers"));
                spel.setMax_Spelers(resultSet.getInt("max_Spelers"));
                spel.setSoortNr(resultSet.getInt("Soortnr"));
                spel.setSpeelDuur(resultSet.getString("speelduur"));
                spel.setMoeilijkheidNr(resultSet.getInt("moeilijkheidnr"));
                spel.setPrijs(resultSet.getDouble("prijs"));
                spel.setAfbeelding(resultSet.getString("afbeelding"));

                Soort soort = new Soort();
                soort.setNr(resultSet.getInt("nr"));
                soort.setSoortNaam(resultSet.getString("soortnaam"));

                spel.setSoort(soort);

                Moeilijkheid moeilijkheid = new Moeilijkheid();
                moeilijkheid.setNr(resultSet.getInt("nr"));
                moeilijkheid.setMoeilijkheidsNaam(resultSet.getString("moeilijkheidnaam"));
                
                spel.setMoeilijkheid(moeilijkheid);
                return spel;
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new SpelException("Game could not be found.", e);
        }
    }
    public ArrayList <Spel> findSpelList() throws SQLException {
        ArrayList <Spel> spelList = new ArrayList <> ();
        try (Connection connection = createConnection()) {
            PreparedStatement pstatement = connection.prepareStatement("Select * from Spel inner join Soort on spel.soortnr = soort.nr inner join moeilijkheid on spel.moeilijkheidnr = moeilijkheid.nr Order by naam asc");
            ResultSet resultSet = pstatement.executeQuery();

            while (resultSet.next()) {
                Spel spel = new Spel();
                spel.setNr(resultSet.getInt("nr"));
                spel.setNaam(resultSet.getString("naam"));
                spel.setUitgever(resultSet.getString("Uitgever"));
                spel.setAuteur(resultSet.getString("Auteur"));
                spel.setJaarUitgifte(resultSet.getInt("Jaar_uitgifte"));
                spel.setLeeftijd(resultSet.getString("leeftijd"));
                spel.setMin_Spelers(resultSet.getInt("min_Spelers"));
                spel.setMax_Spelers(resultSet.getInt("max_Spelers"));
                spel.setSoortNr(resultSet.getInt("Soortnr"));
                spel.setSpeelDuur(resultSet.getString("speelduur"));
                spel.setMoeilijkheidNr(resultSet.getInt("moeilijkheidnr"));
                spel.setPrijs(resultSet.getDouble("prijs"));
                spel.setAfbeelding(resultSet.getString("afbeelding"));

                Soort soort = new Soort();
                soort.setNr(resultSet.getInt("nr"));
                soort.setSoortNaam(resultSet.getString("soortnaam"));

                spel.setSoort(soort);

                Moeilijkheid moeilijkheid = new Moeilijkheid();
                moeilijkheid.setNr(resultSet.getInt("nr"));
                moeilijkheid.setMoeilijkheidsNaam(resultSet.getString("moeilijkheidnaam"));
                
                spel.setMoeilijkheid(moeilijkheid);
                spelList.add(spel);
            }
            return spelList;
        } catch (SQLException e) {
            throw new SpelException("Game could not be found.", e);
        }
    }
    public ArrayList <Spel> findSpelListByNr() throws SQLException {
        ArrayList <Spel> spelList = new ArrayList <> ();
        try (Connection connection = createConnection()) {
            PreparedStatement pstatement = connection.prepareStatement("Select * from Spel inner join Soort on spel.soortnr = soort.nr inner join moeilijkheid on spel.moeilijkheidnr = moeilijkheid.nr Order by spel.nr asc");
            ResultSet resultSet = pstatement.executeQuery();

            while (resultSet.next()) {
                Spel spel = new Spel();
                spel.setNr(resultSet.getInt("nr"));
                spel.setNaam(resultSet.getString("naam"));
                spel.setUitgever(resultSet.getString("Uitgever"));
                spel.setAuteur(resultSet.getString("Auteur"));
                spel.setJaarUitgifte(resultSet.getInt("Jaar_uitgifte"));
                spel.setLeeftijd(resultSet.getString("leeftijd"));
                spel.setMin_Spelers(resultSet.getInt("min_Spelers"));
                spel.setMax_Spelers(resultSet.getInt("max_Spelers"));
                spel.setSoortNr(resultSet.getInt("Soortnr"));
                spel.setSpeelDuur(resultSet.getString("speelduur"));
                spel.setMoeilijkheidNr(resultSet.getInt("moeilijkheidnr"));
                spel.setPrijs(resultSet.getDouble("prijs"));
                spel.setAfbeelding(resultSet.getString("afbeelding"));

                Soort soort = new Soort();
                soort.setNr(resultSet.getInt("nr"));
                soort.setSoortNaam(resultSet.getString("soortnaam"));

                spel.setSoort(soort);

                Moeilijkheid moeilijkheid = new Moeilijkheid();
                moeilijkheid.setNr(resultSet.getInt("nr"));
                moeilijkheid.setMoeilijkheidsNaam(resultSet.getString("moeilijkheidnaam"));
                
                spel.setMoeilijkheid(moeilijkheid);
                spelList.add(spel);
            }
            return spelList;
        } catch (SQLException e) {
            throw new SpelException("Game could not be found.", e);
        }
    }
    
    public ArrayList <Spel> findSpelMoeilijkheid(int diff) throws SQLException {
        ArrayList <Spel> spelList = new ArrayList <> ();
        try (Connection connection = createConnection()) {
            PreparedStatement pstatement = connection.prepareStatement("Select * from Spel inner join Soort on spel.soortnr = soort.nr inner join moeilijkheid on spel.moeilijkheidnr = moeilijkheid.nr Where spel.moeilijkheidnr >= ? Order by naam asc");
            pstatement.setInt(1, diff);
            
            ResultSet resultSet = pstatement.executeQuery();

            while (resultSet.next()) {
                Spel spel = new Spel();
                spel.setNr(resultSet.getInt("nr"));
                spel.setNaam(resultSet.getString("naam"));
                spel.setUitgever(resultSet.getString("Uitgever"));
                spel.setAuteur(resultSet.getString("Auteur"));
                spel.setJaarUitgifte(resultSet.getInt("Jaar_uitgifte"));
                spel.setLeeftijd(resultSet.getString("leeftijd"));
                spel.setMin_Spelers(resultSet.getInt("min_Spelers"));
                spel.setMax_Spelers(resultSet.getInt("max_Spelers"));
                spel.setSoortNr(resultSet.getInt("Soortnr"));
                spel.setSpeelDuur(resultSet.getString("speelduur"));
                spel.setMoeilijkheidNr(resultSet.getInt("moeilijkheidnr"));
                spel.setPrijs(resultSet.getDouble("prijs"));
                spel.setAfbeelding(resultSet.getString("afbeelding"));

                Soort soort = new Soort();
                soort.setNr(resultSet.getInt("nr"));
                soort.setSoortNaam(resultSet.getString("soortnaam"));

                spel.setSoort(soort);

                Moeilijkheid moeilijkheid = new Moeilijkheid();
                moeilijkheid.setNr(resultSet.getInt("nr"));
                moeilijkheid.setMoeilijkheidsNaam(resultSet.getString("moeilijkheidnaam"));
                
                spel.setMoeilijkheid(moeilijkheid);
                spelList.add(spel);
            }
            return spelList;
        } catch (SQLException e) {
            throw new SpelException("Game could not be found.", e);
        }
    }
    
    public Spel searchSpelonID(int id) throws SQLException {
        try (Connection connection = createConnection()) {
            PreparedStatement pstatement = connection.prepareStatement("Select * from Spel inner join Soort on spel.soortnr = soort.nr inner join moeilijkheid on spel.moeilijkheidnr = moeilijkheid.nr where spel.nr = ?");
            pstatement.setInt(1, id);
            ResultSet resultSet = pstatement.executeQuery();
            if (resultSet.next()) {
                Spel spel = new Spel();
                spel.setNr(resultSet.getInt("nr"));
                spel.setNaam(resultSet.getString("naam"));
                spel.setUitgever(resultSet.getString("Uitgever"));
                spel.setAuteur(resultSet.getString("Auteur"));
                spel.setJaarUitgifte(resultSet.getInt("Jaar_uitgifte"));
                spel.setLeeftijd(resultSet.getString("leeftijd"));
                spel.setMin_Spelers(resultSet.getInt("min_Spelers"));
                spel.setMax_Spelers(resultSet.getInt("max_Spelers"));
                spel.setSoortNr(resultSet.getInt("Soortnr"));
                spel.setSpeelDuur(resultSet.getString("speelduur"));
                spel.setMoeilijkheidNr(resultSet.getInt("moeilijkheidnr"));
                spel.setPrijs(resultSet.getDouble("prijs"));
                spel.setAfbeelding(resultSet.getString("afbeelding"));

                Soort soort = new Soort();
                soort.setNr(resultSet.getInt("nr"));
                soort.setSoortNaam(resultSet.getString("soortnaam"));

                spel.setSoort(soort);

                Moeilijkheid moeilijkheid = new Moeilijkheid();
                moeilijkheid.setNr(resultSet.getInt("nr"));
                moeilijkheid.setMoeilijkheidsNaam(resultSet.getString("moeilijkheidnaam"));
                
                spel.setMoeilijkheid(moeilijkheid);
                return spel;
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new SpelException("Game could not be found.", e);
        }
    }
    
    /*public Spel testExist() throws SQLException{
        Spel spel = new Spel();
        try (Connection connection = createConnection()){
            PreparedStatement pstatement = connection.prepareStatement("Select ");
        }
    }*/
}
